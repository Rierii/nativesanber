import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

class Login extends React.Component{
  render(){
    return(
      <View style={styles.container}>
        <View style={styles.topBar}>
        <TouchableOpacity>
          <Icon  name="arrow-back" size= {40} style={styles.navItems}/>
        </TouchableOpacity>
        </View>

        <View style={styles.body}>
          <Text style={styles.bodyItem}> Username: </Text>
            <View style={styles.box}>
            </View>
          <Text style={styles.bodyItem}> Password: </Text>
            <View style={styles.box}>
            </View>
        </View>
        <View style={styles.botMenu}>
          <View style={styles.buttons}>
            <Text style={styles.buttonText}> Log Me In! </Text>
          </View>
          <Text style={styles.bodyItem}> Or </Text>
            <View style={styles.loginOptions}>
            <TouchableOpacity>
              <Image source={require('./Assets/Git.png')} style={{ width: 30, height: 22 }} />
            </TouchableOpacity>
            <TouchableOpacity>
              <Image source={require('./Assets/Google.png')} style={{ width: 30, height: 22 }} />
            </TouchableOpacity>
            </View>
        </View>


      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'

  },
  body:{
    paddingTop: 20,
    paddingHorizontal: 25,


  },
  bodyItem:{
    fontSize: 20,

  },
  topBar: {
    height: 75,
    backgroundColor: '#1d8ac0',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',

  },
  navItems:{
    paddingTop: 10,
  },
  box: {
    backgroundColor: '#fff',
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#003366',
    padding: 14,
    margin: 20,
  },
  buttons:{
    borderRadius: 10,
    backgroundColor: '#81E0EC',
    height: 35,
    width: 150,
    alignItems: 'center'
  },
  botMenu:{
    justifyContent: 'center',
    alignItems: 'center',

  },
  buttonText:{
    fontSize: 20,
  },
  loginOptions:{
    height: 50,
    backgroundColor: '#1d8ac0',
    elevation: 3,
    width: 355,
    flexDirection: 'row',
    alignItems: 'center',
    borderTopWidth: 0.5,
    borderColor: '#E5E5E5',
    justifyContent: 'space-around'
  }


});

export default Login;
