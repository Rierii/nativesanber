import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

class About extends React.Component{
  render(){
    return(
      <View style={styles.container}>
        <View style={styles.navBar}>
          <Text style={styles.navText}> Hi Sopan! </Text>
            <View style={styles.rightNav}>
              <TouchableOpacity>
                <Icon  name="menu" size= {30} style={styles.navItem}/>
              </TouchableOpacity>
            </View>
        </View>
        <View style={styles.topMenu}>
          <TouchableOpacity>
            <Icon  name="person-outline" size= {80} style={styles.navItem}/>
          </TouchableOpacity>
          <Text style={{fontSize:20}}> Sopan Sopian</Text>
        </View>
        <View style={styles.body}>
          <View style={styles.box}>
            <Image source={require('./Assets/Facebook.png')} style={{ width: 35, height: 35 }} />
            <Text style={{fontSize: 20,paddingHorizontal: 8}}> Sopan Sopians</Text>
          </View>
        </View>
        <View style={styles.body}>
          <View style={styles.box}>
            <Image source={require('./Assets/Twitter.png')} style={{ width: 35, height: 35 }} />
            <Text style={{fontSize: 20,paddingHorizontal: 8}}> @SopanSapiens </Text>
          </View>
        </View>
        <View style={styles.body}>
          <View style={styles.box}>
            <Image source={require('./Assets/Insta.png')} style={{ width: 35, height: 35 }} />
            <Text style={{fontSize: 20,paddingHorizontal: 8}}> @SopanSapiens </Text>
          </View>
        </View>
        <View style={styles.body}>
          <View style={styles.box}>
            <Image source={require('./Assets/GitHub.png')} style={{ width: 35, height: 35 }} />
            <Text style={{fontSize: 20,paddingHorizontal: 8}}> @SopanSapiens </Text>
          </View>
        </View>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'

  },
  navText:{
    fontSize: 20,
  },
  topMenu:{
    height: 150,
    backgroundColor: '#1d8ac0',
    paddingHorizontal: 15,

    alignItems: 'center',
    justifyContent: 'center',
  },
  navBar: {
    backgroundColor: '#1d8ac0',
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  rightNav: {
    flexDirection: 'row'
  },
  body: {
    padding: 15,


  },
  box: {
    backgroundColor: '#81E0EC',
    height: 35,
    borderRadius: 15,
    paddingHorizontal: 10,
    flexDirection: 'row',

  }

});



export default About;
